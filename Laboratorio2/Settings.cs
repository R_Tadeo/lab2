﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laboratorio2
{
    public class Settings
    {
        private int v;

        public Settings()
        {
        }

        public Settings(int v)
        {
            this.v = v;
        }

        public string DotnetExPath { get; set; }
        public string FunctionHostPath { get; set; }
        public string FunctionApplicationPath { get; set; }
        public string AzureWebJobsStorage { get; set; }

    }
}
