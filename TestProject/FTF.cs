﻿using System;
using TestProject;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using Laboratorio2;

namespace TestProject
{
    public class FTF : IDisposable
    {
        private Process _funtionprocess;
        private int _port = 7071;
        public HttpClient Client = new HttpClient();

        public FTF()
        {
            var dotnet = Environment.ExpandEnvironmentVariables(ConfigHelper.Settings.DotnetExPath);
            var functionHost = Environment.ExpandEnvironmentVariables(ConfigHelper.Settings.FunctionHostPath);
            var folderFunction = Environment.ExpandEnvironmentVariables(ConfigHelper.Settings.FunctionApplicationPath);

            _funtionprocess = new Process
            {
                StartInfo =
                {
                    FileName = dotnet,
                    Arguments = $"\"{functionHost}\" start -p {_port}",
                    WorkingDirectory = folderFunction }
            };

            var success = _funtionprocess.Start();
            Client.BaseAddress = new Uri($"http://localhost:{_port}");
        }

        public void Dispose()
        {
            if (!_funtionprocess.HasExited)
            {
                _funtionprocess.Kill();
            }
            _funtionprocess.Dispose();
        }
    }
}
