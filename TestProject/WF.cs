﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestProject
{
    [Collection(nameof(FTC))]
    public class WF
    {
        private FTF testFixture;
        private HttpResponseMessage responseMessage;

        public WF(FTF fixture)
        {
            testFixture = fixture;
        }

        [Fact]
        public async Task TestWhenFunctionisInvoked()
        {
            responseMessage = await testFixture.Client.GetAsync("api/Welcome?name=Tadeo+M");
            Assert.True(responseMessage.IsSuccessStatusCode);
        }

        [Fact]
        public async Task TestResponseEnd()
        {
            responseMessage = await testFixture.Client.GetAsync("api/Welcome?name=Tadeo+M");
            Assert.EndsWith("sucess", await responseMessage.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task Post_DeleteAllMessagesHandler_ReturnsRedirectToRoot()
        {
            responseMessage = await testFixture.Client.GetAsync("/");
            Assert.True(responseMessage.IsSuccessStatusCode);
        }

    }
}
